using System;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

[Serializable]
public abstract class ConfigHelper<T> where T : Config
{
    public string URL => _config.URL;
    public string PathToConfig => _pathToConfig;

    [SerializeField]
    protected string _pathToConfig;

    protected T _config;

    public virtual void Initialize(string pathToConfig)
    {
        _pathToConfig = pathToConfig;

        var configExists = File.Exists(PathToConfig);

        if (configExists)
            TryDeserializeConfig();
        else
            CreateDefaultConfig();
    }

    private void TryDeserializeConfig()
    {
        try
        {
            var json = File.ReadAllText(PathToConfig);
            _config = JsonConvert.DeserializeObject<T>(json);
        }
        catch (Exception e)
        {
            Debug.LogWarning("Error deserializing config file: " + e.Message);
            CreateDefaultConfig();
        }
    }

    protected abstract void CreateDefaultConfig();

    public void SaveConfig()
    {
        var json = JsonConvert.SerializeObject(_config, Formatting.Indented);
        try
        {
            File.WriteAllText(PathToConfig, json);
        }
        catch (UnauthorizedAccessException)
        {
            Debug.Log("Cant create config file! File system access denied!");
        }
    }
}