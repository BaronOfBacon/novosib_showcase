using System.IO;
using UnityEngine;

public static class ButtonsTexturesLoadHelper
{
    public static Texture2D[] LoadPNGTextures()
    {
        Texture2D[] pngTextures = new Texture2D[12]; // Array to store the loaded PNGs

        string streamingAssetsPath = Application.streamingAssetsPath;

        for (int i = 1; i <= 12; i++)
        {
            string filePath = Path.Combine(streamingAssetsPath, i.ToString() + ".png");

            if (File.Exists(filePath))
            {
                byte[] fileData = File.ReadAllBytes(filePath);
                Texture2D texture = new Texture2D(2, 2); // Adjust dimensions as needed

                if (texture.LoadImage(fileData))
                {
                    pngTextures[i - 1] = texture;
                }
                else
                {
                    Debug.LogError("Failed to load texture: " + filePath);
                }
            }
            else
            {
                Debug.LogError("File not found: " + filePath);
            }
        }

        return pngTextures;
    }
}