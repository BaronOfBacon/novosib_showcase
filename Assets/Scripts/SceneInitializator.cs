using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SceneInitializator : MonoBehaviour
{
    public Button[] buttons;
    
    private void Awake()
    {
        LoadImagesInfoButtons();
    }
    
    void LoadImagesInfoButtons()
    {
        string streamingAssetsPath = Application.streamingAssetsPath;

        for (int i = 1; i <= buttons.Length; i++)
        {
            string imagePath = Path.Combine(streamingAssetsPath, i + ".png");

            if (File.Exists(imagePath))
            {
                // Load the image as a Texture2D
                byte[] imageBytes = File.ReadAllBytes(imagePath);
                Texture2D texture = new Texture2D(2, 2);
                texture.LoadImage(imageBytes);

                // Set the loaded texture as the sprite for the Image component on the button
                buttons[i - 1].GetComponent<Image>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            }
            else
            {
                Debug.LogError("Image not found: " + imagePath);
            }
        }
    }
}
