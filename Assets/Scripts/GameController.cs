using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private List<VideoClip> _videoClips;
    [SerializeField]
    private List<Button> _buttons = new();
    
    [Header("Buttons")]
    [SerializeField]
    private List<Image> _buttonsImages = new();
    
    [Header("Back button")]
    [SerializeField]
    private float _backButtonDelay;
    [SerializeField]
    private float _buttonDownsizeMultiplier;
    [SerializeField]
    private float _buttonDownsizeDuration;
    [SerializeField]
    private Ease _buttonDownsizeEase;

    [Header("General")]
    [SerializeField]
    private Image _image;
    [SerializeField]
    private Color _normalColor;
    [SerializeField]
    private Color _disabledColor;
    [SerializeField]
    private float _fadeDuration;
    [SerializeField]
    private Ease _fadeInEase;
    [SerializeField]
    private Ease _fadeOutEase;

    [Header("Video players")]
    [SerializeField]
    private VideoPlayer _mainVideoPlayer;
    
    [SerializeField]
    private bool _fromStreamingAssets;

    private UDPCommunicator _communicator;
    private ColorBlock _defaultButtonColors;
    private bool _ignoreEndOnce;
    private bool timerRunning = false;
    private float timerDuration = 20.0f;
    private Texture2D[] _buttonsTextures = new Texture2D[12];
    
    private void Awake()
    {
        _communicator = new UDPCommunicator();
        _communicator.Initialize(Application.streamingAssetsPath + "/UDPConfig.cfg");
        _buttonsTextures = ButtonsTexturesLoadHelper.LoadPNGTextures();
        
        if (_fromStreamingAssets)
            SetPicturesFromAssetsFolder();
        
        _defaultButtonColors = _buttons[0].colors;
    }

    private void SetPicturesFromAssetsFolder()
    {
        for (int i = 0; i < 12; i++)
        {
            var texture = _buttonsTextures[i];
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f);
            
            _buttonsImages[i].sprite = sprite;
        }  
    }

    private void HandleMainVideoEnd(VideoPlayer source)
    {
        if (_ignoreEndOnce)
        {
            _ignoreEndOnce = false;
            return;
        }

        FadeOutButtonsAndImage();
    }

    public void OnPlayVideoClick(int index)
    {
        if (_mainVideoPlayer.isPlaying)
            Debug.LogError("Video is playing already! Can't start another one.");

        SetUpVideoPlayer(index);

        var buttonTransform = _buttonsImages[index].transform;
        ChangeButtonsInteractivity(false);

        AnimateButton(buttonTransform, index, OnPlay);
    }

    private void OnPlay(int index)
    {
        FadeInButtonsAndImage(() =>
        {
            _communicator.SendMessage((index+1).ToString());
            _mainVideoPlayer.enabled = true;
            _mainVideoPlayer.Play();
            StartVideoTimer();
            StartVideoCloseTimer();
        });
    }

    private void StartVideoTimer()
    {
        var sequence = DOTween.Sequence();
        sequence.AppendInterval(_backButtonDelay);
        sequence.Play();
    }
    
    private void AnimateButton(Transform buttonTransform, int index, Action<int> onCompleteHook)
    {
        var sequence = DOTween.Sequence();

        sequence.Append(buttonTransform.transform.DOScale(_buttonDownsizeMultiplier, _buttonDownsizeDuration)
                                       .SetEase(_buttonDownsizeEase)
                                       .OnComplete(() => { onCompleteHook?.Invoke(index); }));
        sequence.Append(buttonTransform.transform.DOScale(1, _buttonDownsizeDuration)
                                       .SetEase(_buttonDownsizeEase));
    }

    private void ChangeButtonsInteractivity(bool status)
    {
        foreach (var button in _buttons) button.interactable = status;
    }

    public void OnBackToMenuClick()
    {
        _ignoreEndOnce = true;
        FadeInMainVideoPlayer(FadeOutButtonsAndImage);
    }

    private void FadeInMainVideoPlayer(Action OnCompleteHook)
    {
        var fadeSequence = DOTween.Sequence();
        var time = 0f;
        var tween = DOTween.To(() => time, x => time = x, 0, _fadeDuration)
                           .OnUpdate(() => { _mainVideoPlayer.targetCameraAlpha = 1 - time / _fadeDuration; });
        fadeSequence.Append(tween);
        fadeSequence.OnComplete(() =>
        {
            _mainVideoPlayer.enabled = false;
            _mainVideoPlayer.targetCameraAlpha = 1;
            OnCompleteHook?.Invoke();
        });
        fadeSequence.Play();
    }

    private void FadeInButtonsAndImage(Action OnCompleteHook)
    {
        var fadeSequence = DOTween.Sequence();
        var color = _normalColor;

        var tween = DOTween.To(() => color.a, a => color.a = a, _disabledColor.a, _fadeDuration)
                           .OnUpdate(() =>
                           {
                               foreach (var image in _buttonsImages) ChangeButtonColor(image, color);

                               _image.color = color;
                           }).OnComplete(() =>
                           {
                               foreach (var image in _buttonsImages) image.gameObject.SetActive(false);
                               _image.gameObject.SetActive(false);
                           });
        
        tween.SetEase(_fadeOutEase);
        fadeSequence.Append(tween);
        if (OnCompleteHook != null)
            fadeSequence.OnComplete(OnCompleteHook.Invoke);
    }

    private void FadeOutButtonsAndImage()
    {
        var fadeSequence = DOTween.Sequence();
        var color = _disabledColor;


        var i = 0;
        foreach (var button in _buttons)
        {
            ChangeButtonColor(_buttonsImages[i], _disabledColor);
            button.gameObject.SetActive(true);
            button.interactable = false;
            i++;
        }

        _image.gameObject.SetActive(true);

        var tween = DOTween.To(() => color.a, a => color.a = a, _normalColor.a, _fadeDuration)
                           .OnUpdate(() =>
                           {
                               var j = 0;
                               foreach (var button in _buttons)
                               {
                                   ChangeButtonColor(_buttonsImages[j], color);
                                   j++;
                               }

                               _image.color = color;
                           });

        tween.SetEase(_fadeInEase);

        fadeSequence.Append(tween);
        fadeSequence.OnComplete(() =>
        {
            foreach (var button in _buttons)
            {
                button.colors = _defaultButtonColors;
                button.interactable = true;
            }

            _mainVideoPlayer.enabled = false;
        });
        fadeSequence.Play();
    }

    private void SetUpVideoPlayer(int index)
    {
        _mainVideoPlayer.Stop();

        _mainVideoPlayer.source = _fromStreamingAssets ? VideoSource.Url : VideoSource.VideoClip; 
        
        if (_fromStreamingAssets)
        {
            var videoFilePath = Path.Combine(Application.streamingAssetsPath, $"{index+1}.mov").Replace("\\", "/");
            _mainVideoPlayer.url = videoFilePath;
        }
        else
        {
            _mainVideoPlayer.clip = _videoClips[index];
        }
        
        _mainVideoPlayer.Prepare();
    }

    private void ChangeButtonColor(Image image, Color targetColor)
    {
        image.color = targetColor;
    }
    
    private void StartVideoCloseTimer()
    {
        if (!timerRunning)
        {
            StartCoroutine(TimerCoroutine());
        }
    }
    
    private IEnumerator TimerCoroutine()
    {
        timerRunning = true;
        float elapsedTime = 0f;

        while (elapsedTime < timerDuration)
        {
            elapsedTime += Time.deltaTime;

            yield return null;
        }
        
        HandleMainVideoEnd(null);

        timerRunning = false;
    }
}