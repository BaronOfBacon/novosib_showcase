using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public abstract class NetworkCommunicator<T, TU>
    where T : Config
    where TU : ConfigHelper<T>
{
    public EventHandler<OperationStatusEventArgs> SendMessageHandler;

    public EventHandler<string> MessageReceivedHandler;
    public EventHandler<OperationStatusEventArgs> ListenMessagesHandler;

    protected TU CfgHelper;
    protected bool IsInitialized;

    protected OperationStatus SendMessageStatus;
    protected EventHandler<OperationStatusEventArgs> SendMessageHandlerInternal;

    protected OperationStatus ListenMessagesStatus;
    protected EventHandler<OperationStatusEventArgs> ListenMessagesHandlerInternal;

    protected CancellationTokenSource SendMessageCts;
    protected CancellationTokenSource ListeningMessagesCts;

    public abstract void SendMessage(string message);
    public abstract void StartListening();
    public abstract void StopListening();

    protected abstract Task SendMessageTask(CancellationToken token, string message);
    protected abstract Task ListeningTask(CancellationToken token);

    public virtual void Initialize(string pathToConfig)
    {
        if (IsInitialized)
        {
            Debug.Log("Communicator is already initialized");
            return;
        }

        IsInitialized = true;
        CfgHelper.Initialize(pathToConfig);

        SendMessageStatus = OperationStatus.Ready;
        ListenMessagesStatus = OperationStatus.Ready;

        SendMessageHandlerInternal += HandleSendMessageStatusChanged;
        ListenMessagesHandlerInternal += HandleListenMessagesStatusChanged;

        Debug.Log($"{GetType()} initialized!");
    }

    protected virtual void HandleSendMessageStatusChanged(object o, OperationStatusEventArgs eventArgs)
    {
        SendMessageStatus = eventArgs.Status;

        SendMessageHandler?.Invoke(this, eventArgs);
    }

    protected virtual void HandleListenMessagesStatusChanged(object o, OperationStatusEventArgs eventArgs)
    {
        ListenMessagesStatus = eventArgs.Status;

        ListenMessagesHandler?.Invoke(this, eventArgs);
    }

    ~NetworkCommunicator()
    {
        SendMessageHandlerInternal -= HandleSendMessageStatusChanged;
        ListenMessagesHandlerInternal -= HandleListenMessagesStatusChanged;
    }

    #region Classes

    public class OperationStatusEventArgs : EventArgs
    {
        public OperationStatus Status { get; }
        public string Message { get; private set; }

        public OperationStatusEventArgs(OperationStatus status, string message)
        {
            Status = status;
            Message = message;
        }
    }

    public enum OperationStatus
    {
        Ready,
        Begun,
        Processed,
        Canceled,
        Complete
    }

    #endregion
}