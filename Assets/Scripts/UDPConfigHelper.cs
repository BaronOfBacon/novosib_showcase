public sealed class UDPConfigHelper : ConfigHelper<UDPConfig>
{
    public int ReceivePort
    {
        get => _config.ReceivePort;
        set => _config.ReceivePort = value;
    }

    public int SendPort
    {
        get => _config.SendPort;
        set => _config.SendPort = value;
    }

    protected override void CreateDefaultConfig()
    {
        _config = new UDPConfig
        {
            URL = "127.0.0.1",
            ReceivePort = 11001,
            SendPort = 11002
        };

        SaveConfig();
    }
}