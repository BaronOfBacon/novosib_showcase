using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

[Serializable]
public class UDPCommunicator : NetworkCommunicator<UDPConfig, UDPConfigHelper>
{
    public override void Initialize(string pathToConfig)
    {
        CfgHelper = new UDPConfigHelper();
        base.Initialize(pathToConfig);
    }

    #region Sending

    public override void SendMessage(string message)
    {
        if (SendMessageStatus != OperationStatus.Ready) return;

        SendMessageHandlerInternal.Invoke(this, new OperationStatusEventArgs(OperationStatus.Begun, null));
        SendMessageCts?.Cancel();
        SendMessageCts = new CancellationTokenSource();

        var sendMessageTask = SendMessageTask(SendMessageCts.Token, message);
        Task.Run(sendMessageTask.Start);
    }

    protected override async Task SendMessageTask(CancellationToken token, string message)
    {
        if (string.IsNullOrEmpty(message))
        {
            Debug.LogWarning("You are trying to send empty message");
            return;
        }

        var ipPAddress = IPAddress.Parse(CfgHelper.URL);
        var port = CfgHelper.SendPort;

        using (var udpClient = new UdpClient(port))
        {
            try
            {
                var data = Encoding.UTF8.GetBytes(message);
                var endpoint = new IPEndPoint(ipPAddress, port);

                SendMessageHandlerInternal.Invoke(this, new OperationStatusEventArgs(OperationStatus.Processed, message));
                await udpClient.SendAsync(data, data.Length, endpoint).WithCancellation(token);
                Debug.Log($"Message \"{message}\" sent to {endpoint.Address}:{endpoint.Port}");
            }
            catch (OperationCanceledException)
            {
                SendMessageHandlerInternal.Invoke(this, new OperationStatusEventArgs(OperationStatus.Canceled, message));
                udpClient.Close();
            }
        }

        SendMessageHandlerInternal.Invoke(this,new OperationStatusEventArgs(OperationStatus.Complete, message));
    }

    #endregion

    #region Listening

    public override void StartListening()
    {
        if (ListenMessagesStatus != OperationStatus.Ready) return;

        ListenMessagesHandlerInternal.Invoke(this, new OperationStatusEventArgs(OperationStatus.Begun, null));

        ListeningMessagesCts = new CancellationTokenSource();

        var startListeningTask = ListeningTask(ListeningMessagesCts.Token);
        Task.Run(startListeningTask.Start);
    }

    public override void StopListening()
    {
        if (ListeningMessagesCts.Token.CanBeCanceled)
            ListeningMessagesCts.Cancel();
    }

    protected override async Task ListeningTask(CancellationToken token)
    {
        var port = CfgHelper.ReceivePort;

        using var udpListener = new UdpClient(port);
        
        Debug.Log($"UDP:{port} listening started...");
        ListenMessagesHandlerInternal.Invoke(this, new OperationStatusEventArgs(OperationStatus.Processed, null));

        while (true)
            try
            {
                var result = await udpListener.ReceiveAsync().WithCancellation(token);
                var message = Encoding.UTF8.GetString(result.Buffer);
                MessageReceivedHandler?.Invoke(this, message);

                Debug.Log($"Received message: \"{message}\" from {result.RemoteEndPoint}");
            }
            catch (OperationCanceledException)
            {
                break;
            }

        Debug.Log($"Listening :{port} stopped!");
        ListenMessagesHandlerInternal.Invoke(this, new OperationStatusEventArgs(OperationStatus.Complete, null));
    }

    #endregion

    protected override void HandleListenMessagesStatusChanged(object o, OperationStatusEventArgs eventArgs)
    {
        base.HandleListenMessagesStatusChanged(o, eventArgs);

        if (eventArgs.Status is OperationStatus.Canceled or OperationStatus.Complete)
            HandleListenMessagesStatusChanged(this, new OperationStatusEventArgs(OperationStatus.Ready, null));
    }

    protected override void HandleSendMessageStatusChanged(object o, OperationStatusEventArgs eventArgs)
    {
        base.HandleSendMessageStatusChanged(o, eventArgs);

        if (eventArgs.Status is OperationStatus.Canceled or OperationStatus.Complete)
            HandleSendMessageStatusChanged(this, new OperationStatusEventArgs(OperationStatus.Ready, null));
    }
}